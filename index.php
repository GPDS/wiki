<!DOCTYPE html>

<!-- 
    miniwiki - a minimalist wiki software. (0.1.0 alpha)
    Copyright (C) 2015, Adjamilton Junior (jr@ieee.org). All rights reserved.
	
	Licensed under GPL v2.
-->

<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Rock Paper Scissor Web Challenge">
    <meta name="keywords" content="Rock,Paper,Scissor,RPS,Challenge,Game,IFPB">
    <meta name="author" content="Adjamilton Junior">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Rock-Paper-Scissor Web Challenge</title>

    <!-- Font Awesome (MAX CDN) -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Rancho' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Rancho&effect=shadow-multiple' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>

    <!-- Bootstrap (MAX CDN) -->
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        
     <!-- Bootstrap Material Design (JSDELIVR CDN) -->
    <link href="http://cdn.jsdelivr.net/bootstrap.material-design/0.3.0/css/material-fullpalette.css">

    <!-- Local CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <!-- JQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    
</head> <!-- End of Head -->
<body>
	GPDS
</body>
</html>